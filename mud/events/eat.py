# -*- coding: utf-8 -*-
# Copyright (C) 2017 Astolfi Anthony Hardon Corentin, IUT d'Orléans
#==============================================================================

from .event import Event2

class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.fail()
            return self.inform("eat.failed")
        self.inform("eat")
        self.object.delete(self.actor.container())
